package de.sternico.education.sorter;

import java.util.List;

public class InvertedIntegerSorter extends SimpleIntegerSorter {
	
	@Override
	protected int findIndex(List<Integer> partialResult, Integer aNextValue) {
		for (int i = 0; i < partialResult.size(); ++i) {
			Integer iValue = partialResult.get(i);
			if (aNextValue > iValue) {
				return i;
			}
		}
		
		return partialResult.size();
	}
	
}
