package de.sternico.education.sorter;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Test;

public class LimitedIntegerSorterTest {

	private final LimitedIntegerSorter sorter = new LimitedIntegerSorter();
	
	private final Random random = new Random();
	
	@Test
	public void testNullAndEmpty() {
		List result;
		
		result = sorter.sort(null);
		assertNotNull(result);
		assertEquals(0, result.size());
		
		result = sorter.sort(new ArrayList<Integer>());
		assertNotNull(result);
		assertEquals(0, result.size());
	}
	
	@Test
	public void testMismatchedType() {
		Double[] doubleArray = { 1.0, 2.0, 3.0 };
		
		List result = sorter.sort(new ArrayList<Double>(Arrays.asList(doubleArray)));
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	@Test
	public void testSort() {
		// Test randomly generated lists ranging from sizes 1 to 5.
		for (int listSize = 1; listSize <= 5; ++listSize) {
			ArrayList<Integer> list = new ArrayList<Integer>();
			
			// Create list with listSize elements.
			for (int i = 0; i < listSize; ++i) {
				list.add(random.nextInt());
			}
			
			sorter.sort(list);
			
			assertEquals(listSize, list.size());
			
			// Check if list is sorted.
			int previousInt = list.get(0);
			for (int i = 1; i < list.size(); ++i) {
				assertTrue(previousInt <= list.get(i));
			}
		}
	}
	
	@Test(expected = SorterException.class)
	public void testSorterException1() {
		// Array of size 6.
		Integer[] intArray = { 1, 3, 6, 2, 5, 4 };
		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(intArray));
		
		// This should throw the exception.
		sorter.sort(list);

		// List should remain unsorted.
		assertArrayEquals(intArray, list.toArray());
	}
	
	@Test(expected = SorterException.class)
	public void testSorterException2() {
		// Array of size 7.
		Integer[] intArray = { 7, 1, 3, 6, 2, 5, 4 };
		ArrayList<Integer> list = new ArrayList<Integer>(Arrays.asList(intArray));
		
		// This should throw the exception.
		sorter.sort(list);

		// List should remain unsorted.
		assertArrayEquals(intArray, list.toArray());
	}

}
