package de.sternico.education.sorter;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Test;

public class SimpleStringSorterTest {

	private final SimpleStringSorter sorter = new SimpleStringSorter();
	
	private final Random random = new Random();
	
	@Test
	public void testNullAndEmpty() {
		List result;
		
		result = sorter.sort(null);
		assertNotNull(result);
		assertEquals(0, result.size());
		
		result = sorter.sort(new ArrayList<String>());
		assertNotNull(result);
		assertEquals(0, result.size());
	}
	
	@Test
	public void testMismatchedType() {
		Double[] doubleArray = { 1.0, 2.0, 3.0 };
		
		List result = sorter.sort(new ArrayList<Double>(Arrays.asList(doubleArray)));
		assertNotNull(result);
		assertEquals(0, result.size());
	}
	
	@Test
	public void testEmptyStrings() {
		String[] unsortedArray = { "beta", "", "gamma", "", "alpha" };
		String[] sortedArray = { "", "", "alpha", "beta", "gamma" };
		
		List result = sorter.sort(new ArrayList<String>(Arrays.asList(unsortedArray)));
		assertArrayEquals(sortedArray, result.toArray());
	}

	@Test
	public void testSort() {
		// Test randomly generated lists ranging from sizes 1 to 20.
		for (int listSize = 1; listSize <= 20; ++listSize) {
			ArrayList<String> list = new ArrayList<String>();
			
			// Create list with listSize elements.
			for (int i = 0; i < listSize; ++i) {
				list.add(generateRandomString());
			}
			
			sorter.sort(list);
			
			assertEquals(listSize, list.size());
			
			// Check if list is sorted.
			String previousString = list.get(0);
			for (int i = 1; i < list.size(); ++i) {
				assertTrue(previousString.compareTo(list.get(i)) < 0);
			}
		}
	}
	
	private String generateRandomString() {
		final int maxLength = 20;
		
		int length = random.nextInt(maxLength) + 1;
		String result = "";
		
		for (int i = 0; i < length; ++i) {
			int randomInt = random.nextInt(26);
			boolean isUppercase = random.nextInt(2) == 1;
			result += isUppercase ? (char) ((int) 'A' + randomInt) : (char) ((int) 'a' + randomInt);
		}
		
		return result;
	}
	
}
