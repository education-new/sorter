package de.sternico.education.sorter;

import java.util.ArrayList;
import java.util.List;

public class SimpleStringSorter implements ISorter {

	@Override
	public <T> List<T> sort(List<T> aUnsorted) {
		if (aUnsorted == null || aUnsorted.size() == 0) {
			return new ArrayList<T>();
		}
		
		for (T element : aUnsorted) {
			if (!(element instanceof String)) {
				return new ArrayList<T>();
			}
		}
		
		ArrayList<String> newList = new ArrayList<String>((List<String>) aUnsorted);
		
		for (int i = 1; i < newList.size(); ++i) {
			String nextValue = newList.get(i);
			int insertIndex = findIndex(newList.subList(0, i), nextValue);
			newList.remove(i);
			newList.add(insertIndex, nextValue);
		}
		
		return (List<T>) newList;
	}
	
	protected int findIndex(List<String> partialResult, String aNextValue) {
		for (int i = 0; i < partialResult.size(); ++i) {
			String iValue = partialResult.get(i);
			if (aNextValue.compareTo(iValue) < 0) {
				return i;
			}
		}
		
		return partialResult.size();
	}
	
}
