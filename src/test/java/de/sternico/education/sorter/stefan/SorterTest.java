package de.sternico.education.sorter.stefan;

import java.util.ArrayList;
import java.util.Arrays;

import org.junit.Assert;
import org.junit.Test;

import de.sternico.education.sorter.InvertedIntegerSorter;
import de.sternico.education.sorter.LimitedIntegerSorter;
import de.sternico.education.sorter.SimpleIntegerSorter;
import de.sternico.education.sorter.SimpleStringSorter;

public class SorterTest
{
    @Test
    public void testSorterEmtpyInput()
    {
        // normal
        Assert.assertEquals(new ArrayList<Integer>(), new SimpleIntegerSorter().sort(null));
        Assert.assertEquals(new ArrayList<Integer>(), new SimpleIntegerSorter().sort(new ArrayList<Integer>()));

        // inverted
        Assert.assertEquals(new ArrayList<Integer>(), new InvertedIntegerSorter().sort(null));
        Assert.assertEquals(new ArrayList<Integer>(), new InvertedIntegerSorter().sort(new ArrayList<Integer>()));

        // limited
        Assert.assertEquals(new ArrayList<Integer>(), new LimitedIntegerSorter().sort(null));
        Assert.assertEquals(new ArrayList<Integer>(), new LimitedIntegerSorter().sort(new ArrayList<Integer>()));

        // string
        Assert.assertEquals(new ArrayList<String>(), new SimpleStringSorter().sort(null));
        Assert.assertEquals(new ArrayList<String>(), new SimpleStringSorter().sort(new ArrayList<String>()));
    }

    @Test
    public void testSorterNonEmtpyInput()
    {
        SimpleIntegerSorter sorter = new SimpleIntegerSorter();

        Assert.assertEquals(Arrays.asList(new Integer[]
        {
                2, 4, 7
        }), sorter.sort(Arrays.asList(new Integer[]
        {
                4, 2, 7
        })));

        Assert.assertEquals(Arrays.asList(new Integer[]
        {
                1, 4, 56, 82, 194, 838, 20056
        }), sorter.sort(Arrays.asList(new Integer[]
        {
                56, 82, 1, 194, 4, 20056, 838
        })));
    }

    @Test
    public void testInvertedSorterNonEmtpyInput()
    {
        InvertedIntegerSorter sorter = new InvertedIntegerSorter();

        Assert.assertEquals(Arrays.asList(new Integer[]
        {
                7, 4, 2
        }), sorter.sort(Arrays.asList(new Integer[]
        {
                4, 2, 7
        })));

        Assert.assertEquals(Arrays.asList(new Integer[]
        {
                20056, 838, 194, 82, 56, 4, 1
        }), sorter.sort(Arrays.asList(new Integer[]
        {
                56, 82, 1, 194, 4, 20056, 838
        })));
    }

    @Test(expected = Exception.class)
    public void testLimitedSorterNonEmtpyInput()
    {
        LimitedIntegerSorter sorter = new LimitedIntegerSorter();

        sorter.sort(Arrays.asList(new Integer[]
        {
                1, 2, 3, 4, 5, 6
        }));
    }

    @Test
    public void testStringSorterNonEmtpyInput()
    {
        SimpleStringSorter sorter = new SimpleStringSorter();

        Assert.assertEquals(Arrays.asList(new String[]
        {
                "a", "b", "c"
        }), sorter.sort(Arrays.asList(new String[]
        {
                "b", "a", "c"
        })));
    }
}