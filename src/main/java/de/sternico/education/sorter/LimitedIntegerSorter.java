package de.sternico.education.sorter;

import java.util.List;

public class LimitedIntegerSorter extends SimpleIntegerSorter {
	
	private final int limit = 5;
	
	@Override
	public <T> List<T> sort(List<T> aUnsorted) throws SorterException {
		if (aUnsorted != null && aUnsorted.size() > limit)
			throw new SorterException(String.format("List length %d exceeded the limit length of %d", aUnsorted.size(), limit));

		return super.sort(aUnsorted);
	}
	
}
