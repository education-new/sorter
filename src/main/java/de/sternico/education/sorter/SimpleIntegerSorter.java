package de.sternico.education.sorter;

import java.util.ArrayList;
import java.util.List;

public class SimpleIntegerSorter implements ISorter {

	@Override
	public <T> List<T> sort(List<T> aUnsorted) {
		if (aUnsorted == null || aUnsorted.size() == 0) {
			return new ArrayList<T>();
		}
		
		for (T element : aUnsorted) {
			if (!(element instanceof Integer)) {
				return new ArrayList<T>();
			}
		}
		
		ArrayList<Integer> newList = new ArrayList<Integer>((List<Integer>) aUnsorted);
		
		for (int i = 1; i < newList.size(); ++i) {
			Integer nextValue = newList.get(i);
			int insertIndex = findIndex(newList.subList(0, i), nextValue);
			newList.remove(i);
			newList.add(insertIndex, nextValue);
		}
		
		return (List<T>) newList;
	}
	
	protected int findIndex(List<Integer> partialResult, Integer aNextValue) {
		for (int i = 0; i < partialResult.size(); ++i) {
			Integer iValue = partialResult.get(i);
			if (aNextValue < iValue) {
				return i;
			}
		}
		
		return partialResult.size();
	}
	
}
