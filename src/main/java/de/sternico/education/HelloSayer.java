package de.sternico.education;

public class HelloSayer {

	private static final String GREETING = "Hello";

	public String sayHello() {
		return GREETING;
	}
}