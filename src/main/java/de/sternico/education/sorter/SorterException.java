package de.sternico.education.sorter;

public class SorterException extends RuntimeException {

	public SorterException(String message) {
		super(message);
	}
	
}
