package de.sternico.education.sorter;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.junit.Test;

public class SimpleIntegerSorterTest {
	
	private final SimpleIntegerSorter sorter = new SimpleIntegerSorter();
	
	private final Random random = new Random();
	
	@Test
	public void testNullAndEmpty() {
		List result;
		
		result = sorter.sort(null);
		assertNotNull(result);
		assertEquals(0, result.size());
		
		result = sorter.sort(new ArrayList<Integer>());
		assertNotNull(result);
		assertEquals(0, result.size());
	}
	
	@Test
	public void testMismatchedType() {
		Double[] doubleArray = { 1.0, 2.0, 3.0 };
		
		List result = sorter.sort(new ArrayList<Double>(Arrays.asList(doubleArray)));
		assertNotNull(result);
		assertEquals(0, result.size());
	}

	@Test
	public void testSort() {
		// Test randomly generated lists ranging from sizes 1 to 20.
		for (int listSize = 1; listSize <= 20; ++listSize) {
			ArrayList<Integer> list = new ArrayList<Integer>();
			
			// Create list with listSize elements.
			for (int i = 0; i < listSize; ++i) {
				list.add(random.nextInt());
			}
			
			sorter.sort(list);
			
			assertEquals(listSize, list.size());
			
			// Check if list is sorted.
			int previousInt = list.get(0);
			for (int i = 1; i < list.size(); ++i) {
				assertTrue(previousInt <= list.get(i));
			}
		}
	}

}
