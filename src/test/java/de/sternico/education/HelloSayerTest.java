package de.sternico.education;

import org.junit.Assert;
import org.junit.Test;

public class HelloSayerTest {

	@Test
	public void testSayHello() {
		Assert.assertEquals("Hello", new HelloSayer().sayHello());
	}
}