package de.sternico.education.sorter;

import java.util.List;

public interface ISorter {

	public <T> List<T> sort(List<T> aUnsorted);
	
}
